package com.M15_Enum_Number_String;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtils {
  public static void main(String[] args) {
    System.out.println(getFinishingTime(new Date(2020 - 1900, 7, 24), 12));
  }

  public static Date addHoursToDate(Date date, int hours) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    int fullDays = hours / 8;
    int leftHours = hours - (fullDays * 8);
    calendar.add(Calendar.HOUR, (fullDays * 24) + (leftHours > 0 ? leftHours + 10 : 0));
    return calendar.getTime();
  }

  public static boolean isCourseFinished(Date startDate, Integer courseLength) {
    Date finishDate = addHoursToDate(startDate, courseLength);
    return finishDate.compareTo(new Date(System.currentTimeMillis())) < 0;
  }

  public static Date getTodaysMidnight() {
    Date date = new Date();
    Instant inst = date.toInstant();
    LocalDate localDate = inst.atZone(ZoneId.systemDefault()).toLocalDate();
    Instant dayInst = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
    return Date.from(dayInst);
  }

  public static String getFinishingTime(Date startDate, Integer courseLength) {
    StringBuilder res = new StringBuilder();
    Date finishDate = addHoursToDate(startDate, courseLength);
    float hoursBetween = TimeUnit.HOURS.convert(finishDate.getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    float hoursLeft = ((hoursBetween * 8) / 24);
    int workingDaysLeft = (int) hoursLeft / 8;
    int workingHoursLeft = (int) (hoursLeft - (workingDaysLeft * 8));
    if (hoursBetween > 0) {
      res.append("Time left: ");
    } else {
      res.append("Time after course finishing: ");
    }
    if (workingDaysLeft != 0)
      res.append(Math.abs(workingDaysLeft) + "d ");
    if (workingHoursLeft != 0)
      res.append(Math.abs(workingHoursLeft) + "h.");
    return res.toString();
  }

}


