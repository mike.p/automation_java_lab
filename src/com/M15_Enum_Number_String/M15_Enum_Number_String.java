package com.M15_Enum_Number_String;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class M15_Enum_Number_String {

  public static void main(String[] args) {
    for (Students a : Students.values()) {
      if (args.length == 0 || args[0].equals("0")) {
        a.PrintReportAdvanced();
      } else {
        a.PrintReportSimple();
      }
    }
  }
}

enum Courses {
  Java_OOP("Java object oriented programming course.", 43),
  Automation("Automation with Java course.", 56),
  Manual_QA("Manual quality assurance course.", 87),
  DevOps("Dev operations course", 33),
  English("English course.", 55);

  private final String description;
  private final Integer courseLength;

  Courses(String description, int courseLength) {
    this.description = description;
    this.courseLength = courseLength;
  }

  public String getDescription() {
    return this.description;
  }

  public Integer getCourseLength() {
    return this.courseLength;
  }
}

enum Students {
  JavaDeveloper("Aleksandr Pechenig", "6.06.2020", Courses.Java_OOP, Courses.English),
  JavaAutomation("Vladimir Visotskiy", "27.08.2020", Courses.Java_OOP, Courses.Automation, Courses.English),
  Manual_QA("Aleksandr Velikiy", "17.08.2020", Courses.Manual_QA, Courses.English),
  DevOps("Aleksey Bugryaniy", "19.08.2020", Courses.DevOps, Courses.English),
  JavaMaster("Egor Bugayenko", "22.08.2020", Courses.Java_OOP, Courses.Automation,
          Courses.Manual_QA, Courses.English);

  private static final int WorkingDay = 8;
  private Courses[] coursesList;
  private String fio;
  private Date startDate;

  Students(String fio, String startDate, Courses... coursesList) {
    try {
      SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM.yyyy");
      this.fio = fio;
      this.coursesList = coursesList;
      this.startDate = myFormat.parse(startDate);
    } catch (ParseException error) {
      this.startDate = new Date();
    }
  }

  public void PrintStudent() {
    StringBuilder res = new StringBuilder();
    Integer courseDuration = 0;
    res.append("STUDENT: ").append(this.fio).append("\n");
    res.append("CIRRICLULUM: ").append(this.name()).append("\n");
    for (Courses a : this.coursesList) {
      courseDuration += a.getCourseLength();
    }
    res.append("\nCOURSE DURATION ").append(courseDuration).append("\n");
    res.append("----------------------------------").append("\n");
    for (int i = 0; i < this.coursesList.length; i++) {
      res.append(i)
              .append(". ")
              .append(this.coursesList[i].getDescription())
              .append(" ")
              .append(this.coursesList[i].getCourseLength())
              .append("\n");
    }
    System.out.println(res);
  }

  public void PrintReportAdvanced() {
    StringBuilder res = new StringBuilder();
    DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
    long difference = System.currentTimeMillis() - this.startDate.getTime();
    float daysBetween = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
    float hoursToday = TimeUnit.HOURS.convert(System.currentTimeMillis() - DateUtils.getTodaysMidnight().getTime(), TimeUnit.MILLISECONDS);
    int courseDuration = 0;
    for (Courses i : this.coursesList) {
      courseDuration += i.getCourseLength();
    }
    if (hoursToday > 10 && hoursToday <= 18) {
      hoursToday -= 10;
    } else if (hoursToday < 10) {
      hoursToday = 0;
    } else {
      hoursToday = WorkingDay;
    }
    res.append("FIO: ").append(this.fio).append("\n")
            .append("Time spent: ")
            .append(Math.round((difference <= 0 ? courseDuration : (daysBetween * WorkingDay) + hoursToday)) + "h \n")
            .append("CURRICULUM: ")
            .append(this.name() + "\n")
            .append("Course duration: " + courseDuration + "h\n")
            .append("START_DATE: " + df.format(this.startDate) + "\n")
            .append("END_DATE: " + df.format(DateUtils.addHoursToDate(startDate, courseDuration)) + "\n")
            .append(DateUtils.getFinishingTime(this.startDate, courseDuration) + "\n\n");
    System.out.println(res);
  }

  public void PrintReportSimple() {
    StringBuilder res = new StringBuilder();
    int courseDuration = 0;
    for (Courses i : this.coursesList) {
      courseDuration += i.getCourseLength();
    }
    res.append(this.fio).append(" (" + this.name() + ") ")
            .append(DateUtils.isCourseFinished(this.startDate, courseDuration) ? "Course is finished. " : "Course is still running. ")
            .append(DateUtils.getFinishingTime(this.startDate, courseDuration));
    System.out.println(res);
  }
}
