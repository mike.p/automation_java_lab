package com.M18;

import java.util.Comparator;

public class VegetableWeightComparator implements Comparator<Vegetable> {
  @Override
  public int compare(Vegetable one, Vegetable two) {
    return one.getWeight().compareTo(two.getWeight());
  }
}
