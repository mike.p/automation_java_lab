package com.M18;

abstract class Vegetable {
  private final Integer calories;
  private final Integer weight;
  private final String name;

  Vegetable(String name, int weight, int calories) {
    this.name = name;
    this.weight = weight;
    this.calories = calories;

  }

  abstract String preparation();

  protected Integer getCalories() {
    return this.calories;
  }

  protected Integer getWeight() {
    return this.weight;
  }

  protected String getName() {
    return this.name;
  }

}
