package com.M18;

public class Soft extends Vegetable {
  public Soft(String name, int weight, int calories) {
    super(name, weight, calories);
  }

  public String preparation() {
    return "Slice";
  }
}
