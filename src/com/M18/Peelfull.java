package com.M18;

public class Peelfull extends Vegetable {
  public Peelfull(String name, int weight, int calories) {
    super(name, weight, calories);
  }

  public String preparation() {
    return "Clean and slice";
  }

}
