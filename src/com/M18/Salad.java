package com.M18;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Salad {
  String name;
  List<Vegetable> ingredients;

  Salad(String name, Vegetable... components) {
    this.name = name;
    this.ingredients = new ArrayList<>();
    this.ingredients.addAll(Arrays.asList(components));
  }

  public String getName() {
    return name;
  }

  public int foodValue() {
    int v = 0;
    for (Vegetable vegetable : this.ingredients) {
      v += Math.round((double) vegetable.getWeight() * vegetable.getCalories() / 100);
    }
    return v;
  }

  public void sort(String param) {
    switch (param) {
      case "calories":
        this.ingredients.sort(new VegetableCaloriesComparator());
        break;
      case "weight":
        this.ingredients.sort(new VegetableWeightComparator());
        break;
      default:
    }
  }


  @Override
  public String toString() {
    int salcalor = this.foodValue();
    StringBuilder s = new StringBuilder("Salad " + this.getName() + "\n");
    for (Vegetable vegetable : ingredients) {
      s.append("\t").append(vegetable.getName()).append(" \n\t\tweight: ").append(vegetable.getWeight()).append(", calories: ")
              .append(vegetable.getCalories()).append(", prepare: ").append(vegetable.preparation()).append("\n");
    }

    return s + "Total calories " + salcalor + " cal";
  }
}
