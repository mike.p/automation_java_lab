package com.M18;

public class Chief {

  public static void main(String[] args) {
    Salad salad = new Salad("Disguised toast",
            new Solid("Carrot", 100, 25),
            new Peelfull("Onion", 2, 20),
            new Soft("Salat", 1500, 23),
            new Solid("Potato", 200, 100));
    salad.sort("calories");
    System.out.println(salad);
  }
}
