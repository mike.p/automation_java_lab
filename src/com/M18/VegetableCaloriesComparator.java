package com.M18;

import java.util.Comparator;

public class VegetableCaloriesComparator implements Comparator<Vegetable> {
  @Override
  public int compare(Vegetable one, Vegetable two) {
    return one.getCalories().compareTo(two.getCalories());
  }
}
