package com.M18;

public class Solid extends Vegetable {
  public Solid(String name, int weight, int calories) {
    super(name, weight, calories);
  }

  public String preparation() {
    return "Boil, clean";
  }
}
