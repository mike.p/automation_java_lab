package com.M15_TemplateGenerator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.regex.Matcher;

public class TemplateGenerator {
  public static void main(String[] args) {
    try {
      System.out.println("Enter template: ");
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      String templateString = reader.readLine();
      HashMap<String, String> variableMap = processString(templateString); //"asd ${greeting}, ${name}");
      System.out.println("Enter variables: ");
      String variableString = reader.readLine();
      parseVariables(variableMap, variableString);
      for (String a : variableMap.keySet()) {
        templateString = templateString.replaceAll("\\$\\{" + a + "\\}", variableMap.get(a));
      }
      System.out.println(templateString);
    } catch (Exception err) {
      System.out.println(err.getMessage());
    }
  }

  public static HashMap<String, String> processString(String line) {
    HashMap<String, String> variableMap = new HashMap<>();
    Matcher m = TemplatePatterns.TEMPLATE_RECORDER.getPattern().matcher(line);
    while (m.find()) {
      variableMap.put(m.group().substring(2, m.group().length() - 1), "");
    }
    return variableMap;
  }

  private static void parseVariables(HashMap<String, String> variableMap, String line) throws Exception {
    Matcher m = TemplatePatterns.TEMPLATE_VAR_PARSER.getPattern().matcher(line);
    while (m.find()) {
      if (variableMap.get(m.group().substring(0, m.group().indexOf('='))) == null)
        throw new Exception("Error, unable to find template");
      variableMap.put(m.group().substring(0, m.group().indexOf('=')), m.group().substring(m.group().indexOf('=') + 1));
    }
  }

}
