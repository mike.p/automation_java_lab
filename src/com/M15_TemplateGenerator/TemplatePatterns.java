package com.M15_TemplateGenerator;

import java.util.regex.Pattern;

public enum TemplatePatterns {
  TEMPLATE_RECORDER(Pattern.compile("\\$\\{[a-z]+\\}")),
  TEMPLATE_VAR_PARSER(Pattern.compile("[a-z|A-Z|0-9]+\\=[a-z|A-Z|0-9]+"));
  Pattern value;

  TemplatePatterns(Pattern value) {
    this.value = value;
  }

  public Pattern getPattern() {
    return this.value;
  }
}
