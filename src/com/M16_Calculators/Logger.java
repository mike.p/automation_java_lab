package com.M16_Calculators;

public interface Logger {

  default void info(String data) {
    System.out.println(data);
  }
}
