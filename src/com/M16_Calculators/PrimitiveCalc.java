package com.M16_Calculators;

public class PrimitiveCalc implements Logger {
  public static void main(String[] args) {

    PrimitiveCalc Calculator = new PrimitiveCalc();

    Calculator.divide(2, 3);
    Calculator.add(2, 3);
    Calculator.substract(2, 3);
    Calculator.multiply(2, 3);

    Calculator.unifiedAction(2, 3, "/");
    Calculator.unifiedAction(2, 3, "+");
    Calculator.unifiedAction(2, 3, "-");
    Calculator.unifiedAction(2, 3, "*");
    Calculator.unifiedAction(2, 3, "&*)");
  }

  public void divide(Integer a, Integer b) {
    this.info(a + " / " + b + " = " + (a.doubleValue() / b.doubleValue()));
  }

  public void add(Integer a, Integer b) {
    this.info(a + " + " + b + " = " + (a + b));
  }

  public void substract(Integer a, Integer b) {
    this.info(a + " - " + b + " = " + (a - b));
  }

  public void multiply(Integer a, Integer b) {
    this.info(a + " * " + b + " = " + a * b);
  }

  public void unifiedAction(Integer a, Integer b, String action) {
    switch (action) {
      case "+" -> add(a, b);
      case "-" -> substract(a, b);
      case "/" -> divide(a, b);
      case "*" -> multiply(a, b);
      default -> this.info("Unknown operation!");
    }
  }

}
