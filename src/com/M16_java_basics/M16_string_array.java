package com.M16_java_basics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

  /*
  Ввести n строк с консоли, найти самую короткую и самую длинную строки. Вывести найденные строки и их длину.
   */

public class M16_string_array {
  public static void main(String[] args) throws IOException {
    int stringCount = 4;
    System.out.printf("Enter %o strings:\n", stringCount);
    processArrayStrings(readConsoleStrings(4));
  }

  /**
   * @param array array to process
   */
  public static void processArrayStrings(String[] array) {
    HashMap<String, int[]> map = new HashMap<>();
    map.put("max", new int[]{0, array[0].length()});
    map.put("min", new int[]{0, array[0].length()});
    for (int i = 0; i < array.length; i++) {
      System.out.println("String №" + (i + 1) + " is `" + array[i] + "`" + ", length = " + array[i].length());
      if (map.get("max")[1] < array[i].length())
        map.put("max", new int[]{i, array[i].length()});
      if (map.get("min")[1] > array[i].length())
        map.put("min", new int[]{i, array[i].length()});
    }
    System.out.println("Max length is string №" + (map.get("max")[0] + 1) + " which length is " + map.get("max")[1]);
    System.out.println("Min length is string №" + (map.get("min")[0] + 1) + " which length is " + map.get("min")[1]);
  }

  /**
   * @param stringCount strings number to read from console
   * @return array of strings that were read from console
   */
  public static String[] readConsoleStrings(int stringCount) throws IOException {
    String[] arrayOfStrings = new String[stringCount];
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    for (int i = 0; i < arrayOfStrings.length; i++) {
      arrayOfStrings[i] = reader.readLine();
    }
    return arrayOfStrings;
  }
}
