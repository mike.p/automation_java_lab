package com.M16_java_basics;


import java.util.Random;

  /*
  В массиве целых чисел утроить каждый положительный элемент, который стоит перед отрицательным.
   */

public class M16_arrays {
  public static void main(String[] args) {
    int arrayLength = 20;
    System.out.print("Generated Array:\n\t");
    int[] a = generateRandomArray(arrayLength);
    System.out.print("\nModified Array:\n\t");
    processArray(a);
    for (Integer log : a) {
      System.out.print(log + " ");
    }
  }

  /**
   * @param length array length to generate.
   * @return generated array
   */
  public static int[] generateRandomArray(int length) {
    int min = -10;
    int max = 10;
    Random random = new Random();

    int[] res = new int[length];
    for (int i = 0; i < res.length; i++) {
      res[i] = random.nextInt((max - min) + 1) + min;
      System.out.print(res[i] + " ");
    }
    return res;
  }


  /**
   * @param array array to process
   */
  public static void processArray(int[] array) {
    for (int i = 0; i < array.length - 1; i++) {
      if (array[i] > 0 && array[i + 1] < 0) {
        array[i] *= 3;
      }
    }
  }
}
